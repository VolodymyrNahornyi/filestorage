﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Models;

namespace FileStorage.BL.Interfaces
{
    public interface IFileService
    {
        Task<IEnumerable<File>> GetAllFilesAsync();
        File GetFile(int id);
        Task<bool> CreateFile(File file);
        Task<bool> DeleteFile(int id);
        Task<bool> UpdateFile(File file);
    }
}
