﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FileStorage.BL.Dto;
using FileStorage.BL.Infrastructure;

namespace FileStorage.BL.Interfaces
{
    public interface IUserService
    {
        //Task<IEnumerable<UserDto>> GetAllUsersAsync();
        //UserDto GetUser(int id);
        Task<OperationDetails> CreateUser(UserDto userDto);
        //Task<OperationDetails> DeleteUser(int id);
        //Task<OperationDetails> UpdateUser(UserDto userDto);

        Task<ClaimsIdentity> Authenticate(UserDto userDto);
        Task SetInitialData(UserDto adminDto, List<string> roles);
    }
}
