﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FileStorage.BL.Dto;
using FileStorage.BL.Infrastructure;
using FileStorage.BL.Interfaces;
using FileStorage.DAL.Interfaces;
using FileStorage.DAL.Models;
using Microsoft.AspNet.Identity;

namespace FileStorage.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public async Task<OperationDetails> CreateUser(UserDto userDto)
        {
            ApplicationUser user = await _unitOfWork.UserManager.FindByEmailAsync(userDto.Email);

            if (user != null)
                return new OperationDetails(false, "User with this login already exists", "Email");

            user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };

            var result = await _unitOfWork.UserManager.CreateAsync(user, userDto.Password);

            if (result.Errors.Any())
                return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

            // добавляем роль
            await _unitOfWork.UserManager.AddToRoleAsync(user.Id, userDto.Role);

            // создаем профиль клиента
            UserInfo userProfile = new UserInfo()
            {
                UserId = user.Id,
                Name = userDto.Name
            };

            _unitOfWork.Users.CreateUser(userProfile);

            await _unitOfWork.SaveAsync();

            return new OperationDetails(true, "Registration has been completed successfully", "");
        }

        public async Task<ClaimsIdentity> Authenticate(UserDto userDto)
        {
            ClaimsIdentity claim = null;

            // находим пользователя
            ApplicationUser user = await _unitOfWork.UserManager.FindAsync(userDto.Email, userDto.Password);

            // авторизуем его и возвращаем объект ClaimsIdentity
            if (user != null)
                claim = await _unitOfWork.UserManager.CreateIdentityAsync(user,
                    DefaultAuthenticationTypes.ApplicationCookie);

            return claim;
        }

        // начальная инициализация бд
        public async Task SetInitialData(UserDto adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await _unitOfWork.RoleManager.FindByNameAsync(roleName);

                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await _unitOfWork.RoleManager.CreateAsync(role);
                }
            }

            await CreateUser(adminDto);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}