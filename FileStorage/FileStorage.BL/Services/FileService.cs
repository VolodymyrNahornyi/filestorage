﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.BL.Interfaces;
using FileStorage.DAL.Interfaces;
using FileStorage.DAL.Models;

namespace FileStorage.BL.Services
{
    public class FileService : IFileService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FileService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public File GetFile(int id)
        {
            return _unitOfWork.Files.GetFile(id);
        }

        public async Task<bool> CreateFile(File file)
        {
            var result = file != null;

            if (result)
            {
                _unitOfWork.Files.CreateFile(file);
                await _unitOfWork.SaveAsync();
            }

            return result;
        }

        public async Task<bool> DeleteFile(int id)
        {
            var file = _unitOfWork.Files.GetFile(id);
            
            var result = file != null;

            if (result)
            {
                _unitOfWork.Files.DeleteFile(file);
                await _unitOfWork.SaveAsync();
            }

            return result;
        }

        public async Task<bool> UpdateFile(File file)
        {
            var result = file != null;

            if (result)
            {
                _unitOfWork.Files.UpdateFile(file);
                await _unitOfWork.SaveAsync();
            }

            return result;
        }

        public async Task<IEnumerable<File>> GetAllFilesAsync()
        {
            return await _unitOfWork.Files.GetAllFilesAsync();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
