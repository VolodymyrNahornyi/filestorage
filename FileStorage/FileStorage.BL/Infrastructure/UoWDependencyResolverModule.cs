﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Interfaces;
using FileStorage.DAL.UoW;
using Ninject.Modules;

namespace FileStorage.BL.Infrastructure
{
    public class UoWDependencyResolverModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
