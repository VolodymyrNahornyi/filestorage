﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FileStorage.BL.Interfaces;
using FileStorage.DAL.Models;

namespace FileStorage.UI.Controllers
{
    public class FilesController : Controller
    {
        private readonly IFileService _fileService;

        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        // GET: Files
        public async Task<ActionResult> Index()
        {
            var files = await _fileService.GetAllFilesAsync();

            return View(files);
        }

        //GET: Files/Create
        public ActionResult Create()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(File file)
        {
            if (ModelState.IsValid)
            {
                await _fileService.CreateFile(file);

                return RedirectToAction("Index");
            }

            return View();
        }

        //GET: Files/Details/1
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            File file = _fileService.GetFile((int)id);

            if (file == null)
            {
                return HttpNotFound();
            }

            return View(file);
        }

        //GET: Files/Edit/1
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            File file = _fileService.GetFile((int)id);

            if (file == null)
            {
                return HttpNotFound();
            }

            return View(file);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(File file)
        {
            if (ModelState.IsValid)
            {
                await _fileService.UpdateFile(file);

                return RedirectToAction("Index");
            }

            return View();
        }

        //GET: Files/Delete/1
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            File file = _fileService.GetFile((int)id);

            if (file == null)
            {
                return HttpNotFound();
            }

            return View(file);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                await _fileService.DeleteFile(id);

                return RedirectToAction("Index");
            }

            return View();
        }

    }
}