﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FileStorage.BL.Infrastructure;
using FileStorage.UI.Util;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;

namespace FileStorage.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            NinjectModule serviceModule = new DependencyResolverModule();
            NinjectModule uowModule = new UoWDependencyResolverModule();
            var kernel = new StandardKernel(serviceModule, uowModule);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
