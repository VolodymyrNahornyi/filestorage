﻿using FileStorage.BL.Interfaces;
using FileStorage.BL.Services;
using Ninject.Modules;

namespace FileStorage.UI.Util
{
    public class DependencyResolverModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IFileService>().To<FileService>();
            Bind<IUserService>().To<UserService>();
        }
    }
}