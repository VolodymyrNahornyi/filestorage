﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Interfaces;

namespace FileStorage.DAL.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T :class
    {
        private readonly FileStorageDbContext _fileStorageContext;

        protected RepositoryBase(FileStorageDbContext fileStorageContext)
        {
            _fileStorageContext = fileStorageContext;
        }


        public IQueryable<T> FindAll() => _fileStorageContext.Set<T>();

        public IQueryable<T> FindByCondition(Func<T, bool> expression) =>
            _fileStorageContext.Set<T>()
                .Where(expression).AsQueryable();

        public void Create(T item) => _fileStorageContext.Set<T>().Add(item);

        public void Update(T item)
        {
            _fileStorageContext.Set<T>().AddOrUpdate(item);
        }

        public void Delete(T item)
        {
            _fileStorageContext.Set<T>().Remove(item);
        }
    }
}
