﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Models;
using FileStorage.DAL.Interfaces;

namespace FileStorage.DAL.Repositories
{
    public class UserRepository : RepositoryBase<UserInfo>, IUserRepository
    {
        public UserRepository(FileStorageDbContext fileStorageContext) 
            : base(fileStorageContext)
        {
        }


        public async Task<IEnumerable<UserInfo>> GetAllUsersAsync() => 
            await FindAll()
                .ToListAsync();

        public UserInfo GetUser(int id) =>
            FindByCondition(f => f.UserId.Equals(id))
                .SingleOrDefault();

        public void CreateUser(UserInfo user)
        {
            Create(user);
        }

        public void DeleteUser(UserInfo user)
        {
            Delete(user);
        }

        public void UpdateUser(UserInfo user)
        {
            Update(user);
        }
    }
}
