﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Models;
using FileStorage.DAL.Interfaces;

namespace FileStorage.DAL.Repositories
{
    public class FileRepository : RepositoryBase<File>, IFileRepository
    {
        public FileRepository(FileStorageDbContext fileStorageContext) 
            : base(fileStorageContext)
        {
        }


        public async Task<IEnumerable<File>> GetAllFilesAsync() => 
            await FindAll()
                .ToListAsync();

        public File GetFile(int id) =>
            FindByCondition(f => f.FileId.Equals(id))
                .SingleOrDefault();

        public void CreateFile(File file)
        {
            Create(file);
        }

        public void DeleteFile(File file)
        {
            Delete(file);
        }

        public void UpdateFile(File file)
        {
            Update(file);
        }
    }
}
