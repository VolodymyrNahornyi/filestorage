﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Identity;
using FileStorage.DAL.Interfaces;
using FileStorage.DAL.Models;
using FileStorage.DAL.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FileStorage.DAL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FileStorageDbContext _fileStorageContext;
        private IFileRepository _fileRepository;
        private IUserRepository _userRepository;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;


        public UnitOfWork(FileStorageDbContext fileStorageContext)
        {
            _fileStorageContext = fileStorageContext;
        }

        public IFileRepository Files
        {
            get
            {
                if (_fileRepository == null)
                    _fileRepository = new FileRepository(_fileStorageContext);
                return _fileRepository;
            }
        }

        public IUserRepository Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_fileStorageContext);
                return _userRepository;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_fileStorageContext));
                return _userManager;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                if (_roleManager == null)
                    _roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_fileStorageContext));
                return _roleManager;
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _userManager.Dispose();
                    _roleManager.Dispose();
                }
                this.disposed = true;
            }
        }

        public Task SaveAsync() => _fileStorageContext.SaveChangesAsync();
    }
}
