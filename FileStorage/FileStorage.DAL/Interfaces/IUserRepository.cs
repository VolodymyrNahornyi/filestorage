﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Models;

namespace FileStorage.DAL.Interfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserInfo>> GetAllUsersAsync();
        UserInfo GetUser(int id);
        void CreateUser(UserInfo user);
        void DeleteUser(UserInfo user);
        void UpdateUser(UserInfo user);
    }
}
