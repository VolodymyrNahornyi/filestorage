﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Models;

namespace FileStorage.DAL.Interfaces
{
    public interface IFileRepository
    {
        Task<IEnumerable<File>> GetAllFilesAsync();
        File GetFile(int id);
        void CreateFile(File file);
        void DeleteFile(File file);
        void UpdateFile(File file);
    }
}
