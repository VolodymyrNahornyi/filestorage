﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using FileStorage.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FileStorage.DAL
{
    public class FileStorageDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<UserInfo> UsersInfo { get; set; }

        public DbSet<File> Files { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            /*modelBuilder.Entity<UsersFile>()
                .HasKey(x => new {x.UserId, x.FileId});*/
        }
    }
}
