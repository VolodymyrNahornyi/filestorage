﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.DAL.Models
{
    public class UsersFile
    {
        [Key]
        [ForeignKey(nameof(User))]
        [Required]
        public int UserId { get; set; }

        [Key]
        [ForeignKey(nameof(File))]
        [Required]
        public int FileId { get; set; }

        public virtual UserInfo User { get; set; }
        public virtual File File { get; set; }
    }
}
