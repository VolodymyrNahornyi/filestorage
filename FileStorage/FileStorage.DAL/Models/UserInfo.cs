﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.DAL.Models
{
    public class UserInfo
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "User name is a required field.")]
        [StringLength(255)]
        public string Name { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
